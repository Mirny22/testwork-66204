import { createStore } from "vuex" 
import axios from 'axios'


const store = createStore({
    state: {
        posts:[]
    },
     mutations: {
       
        SET_POSTS (state, posts){
            state.posts = posts
        
        }

    },
    actions: {
        GET_POSTS({commit}){
            return axios.get("http://127.0.0.1:8000/api/post").then((posts=>{
                
                commit('SET_POSTS',posts.data)
            }))
        },
        CREATE_POST({}, post){
            return  axios.post('http://127.0.0.1:8000/api/post/create', post ,{ headers: {'Content-Type': 'multipart/form-data'}})
        },
        UPDATE_POST({}, post){
            return  axios.post('http://127.0.0.1:8000/api/post/update', post ,{ headers: {'Content-Type': 'multipart/form-data'}})
        },
        DELETE_POST({}, post){
            return  axios.post('http://127.0.0.1:8000/api/post/delete',post)
        }
      
    },
    getters: {

        POSTS(state) {
            return state.posts;
        },
      
      
    }



})

export default store